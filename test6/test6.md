# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

姓名：蒋国鑫

学号：202010414306

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|



## 表及表空间设计方案

 表空间设计：

 表空间1：用于存储系统表、索引和常用的表。

 表空间2：用于存储大型表和历史数据。 

```sql
-- 创建表空间
CREATE TABLESPACE space1 DATAFILE '/home/oracle/oradata/space1.dbf' SIZE 500M;
CREATE TABLESPACE space2 DATAFILE '/home/oracle/oradata/space2.dbf' SIZE 500M;
```

![pict1](pict1.png)

```sql
-- 创建商品表
 CREATE TABLE products (
    product_id NUMBER PRIMARY KEY,
    product_name VARCHAR2(100) NOT NULL,
    category VARCHAR2(50),
    price NUMBER(10, 2) NOT NULL,
    stock_quantity NUMBER NOT NULL,
    description VARCHAR2(500)
);


-- 创建客户表
   CREATE TABLE customers (
    customer_id NUMBER PRIMARY KEY,
    customer_name VARCHAR2(50) NOT NULL,
    phone VARCHAR2(20)UNIQUE NOT NULL,
    address VARCHAR2(200)
);


-- 创建订单表
CREATE TABLE orders (
    order_id NUMBER PRIMARY KEY,
    customer_id NUMBER,
    order_date DATE NOT NULL,
    total_amount NUMBER(10, 2) NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);


-- 创建订单详情表
CREATE TABLE order_details (
    order_detail_id NUMBER PRIMARY KEY,
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER NOT NULL,
    price NUMBER(10, 2) NOT NULL,
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id)
);

```

![pict2](pict2.png)

## 植入模拟数据

```sql
-- 插入产品数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO products (product_id, product_name, price, stock_quantity)
    VALUES (i, 'Product' || i, ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 500), 0));
  END LOOP;
  COMMIT;
END;
```

![pict3](pict3.png)

## 权限及用户分配方案

```sql
-- 管理员用户
CREATE USER admin IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE, DBA，CREATE SESSION TO admin;

-- 普通用户
CREATE USER user IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO user;
```

![pict4](pict4.png)

## 创建存储过程,每个表模拟5万条数据

```sql
CREATE OR REPLACE PROCEDURE InsertMockData AS
BEGIN
```



### 1.插入顾客信息

```sql
 FOR i IN 1..50000 LOOP
 INSERT INTO Customers (Customerid, CustomerName, CustomerSex, CustomerBirth, CustomerPhone)
 VALUES ('c' || LPAD(i, 6, '0'), 'Customer' || i, CASE MOD(i, 2) WHEN 0 THEN '女' ELSE '男' END, DATE '2000-01-01', '1881234567' || LPAD(i, 3, '0'));
 END LOOP;
```



### 2.插入商品信息

 

```sql
FOR i IN 1..50000 LOOP

 INSERT INTO goods (GoodId, GoodName, GoodPrice, GoodType, GoodStock, GoodState)

 VALUES ('s' || LPAD(i, 6, '0'), 'Good' || i, 1000 + i, CASE MOD(i, 3) WHEN 0 THEN '家电' WHEN 1 THEN '数码' ELSE '手机' END, 100, '出售中');

 END LOOP;
```



### 3.插入订单信息和订单明细

```sql
 FOR i IN 1..50000 LOOP

 INSERT INTO orders (orderid, CustomerId, OrderDate, OrderStatue)

 VALUES (i, 'c' || LPAD(i, 6, '0'), DATE '2023-01-01', CASE MOD(i, 5) WHEN 0 THEN '成功' ELSE '取消' END);

 INSERT INTO orderdetails (orderid, GoodId, OrderNum, OrderPrice)

 VALUES (i, 's' || LPAD(i, 6, '0'), 1, 1000 + i);

 END LOOP;

 COMMIT;

 
 DBMS_OUTPUT.PUT_LINE('成功.');

EXCEPTION

 WHEN OTHERS THEN

 DBMS_OUTPUT.PUT_LINE('失败: ' || SQLERRM);
 
 ROLLBACK;

END;

BEGIN
 insertmockdata;
END;
```

![pict5](pict5.png)

##  创建程序包并添加存储过程和函数 

```sql
CREATE OR REPLACE PACKAGE sales_system_pkg AS

    -- 添加新产品
    PROCEDURE add_product(
        p_product_name IN products.product_name%TYPE,
        p_category IN products.category%TYPE,
        p_price IN products.price%TYPE,
        p_stock_quantity IN products.stock_quantity%TYPE,
        p_description IN products.description%TYPE
    );

    -- 添加新客户
    PROCEDURE add_customer(
        p_customer_name IN customers.customer_name%TYPE,
        p_phone IN customers.phone%TYPE,
        p_address IN customers.address%TYPE
    );

    -- 获取商品总数
    FUNCTION get_total_products RETURN NUMBER;

    -- 获取客户总数
    FUNCTION get_total_customers RETURN NUMBER;

END sales_system_pkg;
-- 创建程序包
CREATE PACKAGE sales_pkg AS
  -- 存储过程：创建订单
  PROCEDURE create_order(customer_id NUMBER, product_id NUMBER, quantity NUMBER);
  
  -- 存储过程：取消订单
  PROCEDURE cancel_order(order_id NUMBER);
  
  -- 函数：计算订单总金额
  FUNCTION calculate_total_amount(order_id NUMBER) RETURN NUMBER;
END sales_pkg;
/

CREATE PACKAGE BODY sales_pkg AS
  -- 存储过程：创建订单
  PROCEDURE create_order(customer_id NUMBER, product_id NUMBER, quantity NUMBER) AS
    price NUMBER;
    total_amount NUMBER;
  BEGIN
    -- 查询商品价格
    SELECT price INTO price FROM products WHERE product_id = product_id;
    
    -- 计算订单总金额
    total_amount := price * quantity;
    
    -- 更新商品库存数量
    UPDATE products SET quantity = quantity - quantity WHERE product_id = product_id;
    
    -- 插入订单表
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (order_id, customer_id, SYSDATE, total_amount);
    
    -- 插入订单详情表
    INSERT INTO order_details (order_id, product_id, quantity, unit_price)
    VALUES (order_id, product_id, quantity, price);
    
    COMMIT;
  END create_order;
  
  -- 存储过程：取消订单
  PROCEDURE cancel_order(order_id NUMBER) AS
  BEGIN
    -- 删除订单详情表中的记录
    DELETE FROM order_details WHERE order_id = order_id;
    
    -- 删除订单表中的记录
    DELETE FROM orders WHERE order_id = order_id;
    
    COMMIT;
  END cancel_order;
  
  -- 函数：计算订单总金额
  FUNCTION calculate_total_amount(order_id NUMBER) RETURN NUMBER AS
    total_amount NUMBER;
  BEGIN
    -- 查询订单详情表中商品的单价和数量，计算订单总金额
    SELECT SUM(quantity * unit_price) INTO total_amount
    FROM order_details
    WHERE order_id = order_id;
    
    RETURN total_amount;
  END calculate_total_amount;
END sales_pkg;
/

CREATE OR REPLACE PACKAGE BODY sales_system_pkg AS

    -- 添加新产品
    PROCEDURE add_product(
        p_product_name IN products.product_name%TYPE,
        p_category IN products.category%TYPE,
        p_price IN products.price%TYPE,
        p_stock_quantity IN products.stock_quantity%TYPE,
        p_description IN products.description%TYPE
    ) IS
    BEGIN
        INSERT INTO products (product_id, product_name, category, price, stock_quantity, description)
        VALUES (SEQ_PRODUCT_ID.NEXTVAL, p_product_name, p_category, p_price, p_stock_quantity, p_description);
    END add_product;

    -- 添加新客户
    PROCEDURE add_customer(
        p_customer_name IN customers.customer_name%TYPE,
        p_phone IN customers.phone%TYPE,
        p_address IN customers.address%TYPE
    ) IS
    BEGIN
        INSERT INTO customers (customer_id, customer_name, phone, address)
        VALUES (SEQ_CUSTOMER_ID.NEXTVAL, p_customer_name, p_phone, p_address);
    END add_customer;

    -- 获取商品总数
    FUNCTION get_total_products RETURN NUMBER IS
        v_total_products NUMBER;
    BEGIN
        SELECT COUNT(*) INTO v_total_products FROM products;
        RETURN v_total_products;
    END get_total_products;

    -- 获取客户总数
    FUNCTION get_total_customers RETURN NUMBER IS
        v_total_customers NUMBER;
    BEGIN
        SELECT COUNT(*) INTO v_total_customers FROM customers;
        RETURN v_total_customers;
    END get_total_customers;

END sales_system_pkg;
```

![pict6](pict6.png)



#### 数据库备份方案

数据库备份是保障数据安全的重要措施。一般来说，数据库备份方案需要考虑以下几个方面：

1. 备份频率：备份的频率应该根据数据更新的频率来定，数据更新频繁的情况下备份的频率也应该相应增加。
2. 增量备份或全量备份：增量备份只备份发生改变的数据，而全量备份则将整个数据库都备份一遍。增量备份节省了时间和存储空间，但恢复时的复杂度高于全量备份。因此需要权衡取舍。
3. 恢复测试：备份的目的是为了在数据库出现问题时能够恢复数据。因此备份方案应该经常进行恢复测试，以确保备份的数据可以正常恢复。
4. 存储位置：备份的数据应该存放在安全可靠的地方，以防止数据丢失或被篡改。
5. 自动化脚本：备份过程应尽可能实现自动化操作，以减少人为错误的发生。

总之，一个合理的数据库备份方案应该结合具体的情况灵活调整，既要保证数据安全可靠，又要考虑备份的效率和成本。同时还需要不断完善和优化备份流程，以适应数据库在日常运营中的变化。
