-- 创建表空间
CREATE TABLESPACE space1 DATAFILE '/home/oracle/oradata/space1.dbf' SIZE 500M;
CREATE TABLESPACE space2 DATAFILE '/home/oracle/oradata/space2.dbf' SIZE 500M;

-- 创建商品表
 CREATE TABLE products (
    product_id NUMBER PRIMARY KEY,
    product_name VARCHAR2(100) NOT NULL,
    category VARCHAR2(50),
    price NUMBER(10, 2) NOT NULL,
    stock_quantity NUMBER NOT NULL,
    description VARCHAR2(500)
);
​
​
-- 创建客户表
   CREATE TABLE customers (
    customer_id NUMBER PRIMARY KEY,
    customer_name VARCHAR2(50) NOT NULL,
    phone VARCHAR2(20)UNIQUE NOT NULL,
    address VARCHAR2(200)
);
​
​
-- 创建订单表
CREATE TABLE orders (
    order_id NUMBER PRIMARY KEY,
    customer_id NUMBER,
    order_date DATE NOT NULL,
    total_amount NUMBER(10, 2) NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);
​
​
-- 创建订单详情表
CREATE TABLE order_details (
    order_detail_id NUMBER PRIMARY KEY,
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER NOT NULL,
    price NUMBER(10, 2) NOT NULL,
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id)
);
​

植入模拟数据
-- 插入产品数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO products (product_id, product_name, price, stock_quantity)
    VALUES (i, 'Product' || i, ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 500), 0));
  END LOOP;
  COMMIT;
END;

权限及用户分配方案
-- 管理员用户
CREATE USER admin IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE, DBA，CREATE SESSION TO admin;
​
-- 普通用户
CREATE USER user IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO user;

创建存储过程,每个表模拟5万条数据
CREATE OR REPLACE PROCEDURE InsertMockData AS
BEGIN
1.插入顾客信息
 FOR i IN 1..50000 LOOP

 INSERT INTO Customers (Customerid, CustomerName, CustomerSex, CustomerBirth, CustomerPhone)

 VALUES ('c' || LPAD(i, 6, '0'), 'Customer' || i, CASE MOD(i, 2) WHEN 0 THEN '女' ELSE '男' END, DATE '2000-01-01', '1881234567' || LPAD(i, 3, '0'));
 END LOOP;
2.插入商品信息
FOR i IN 1..50000 LOOP
​
 INSERT INTO goods (GoodId, GoodName, GoodPrice, GoodType, GoodStock, GoodState)
​
 VALUES ('s' || LPAD(i, 6, '0'), 'Good' || i, 1000 + i, CASE MOD(i, 3) WHEN 0 THEN '家电' WHEN 1 THEN '数码' ELSE '手机' END, 100, '出售中');
​
 END LOOP;
3.插入订单信息和订单明细
 FOR i IN 1..50000 LOOP
​
 INSERT INTO orders (orderid, CustomerId, OrderDate, OrderStatue)
​
 VALUES (i, 'c' || LPAD(i, 6, '0'), DATE '2023-01-01', CASE MOD(i, 5) WHEN 0 THEN '成功' ELSE '取消' END);
​
 INSERT INTO orderdetails (orderid, GoodId, OrderNum, OrderPrice)
​
 VALUES (i, 's' || LPAD(i, 6, '0'), 1, 1000 + i);
​
 END LOOP;
​
 COMMIT;
​
 
 DBMS_OUTPUT.PUT_LINE('成功.');
​
EXCEPTION
​
 WHEN OTHERS THEN
​
 DBMS_OUTPUT.PUT_LINE('失败: ' || SQLERRM);
 
 ROLLBACK;
​
END;
​
BEGIN
 insertmockdata;
END;

创建程序包并添加存储过程和函数 
CREATE OR REPLACE PACKAGE sales_system_pkg AS
​
    -- 添加新产品
    PROCEDURE add_product(
        p_product_name IN products.product_name%TYPE,
        p_category IN products.category%TYPE,
        p_price IN products.price%TYPE,
        p_stock_quantity IN products.stock_quantity%TYPE,
        p_description IN products.description%TYPE
    );
​
    -- 添加新客户
    PROCEDURE add_customer(
        p_customer_name IN customers.customer_name%TYPE,
        p_phone IN customers.phone%TYPE,
        p_address IN customers.address%TYPE
    );
​
    -- 获取商品总数
    FUNCTION get_total_products RETURN NUMBER;
​
    -- 获取客户总数
    FUNCTION get_total_customers RETURN NUMBER;
​
END sales_system_pkg;
-- 创建程序包
CREATE PACKAGE sales_pkg AS
  -- 存储过程：创建订单
  PROCEDURE create_order(customer_id NUMBER, product_id NUMBER, quantity NUMBER);
  
  -- 存储过程：取消订单
  PROCEDURE cancel_order(order_id NUMBER);
  
  -- 函数：计算订单总金额
  FUNCTION calculate_total_amount(order_id NUMBER) RETURN NUMBER;
END sales_pkg;
/
​
CREATE PACKAGE BODY sales_pkg AS
  -- 存储过程：创建订单
  PROCEDURE create_order(customer_id NUMBER, product_id NUMBER, quantity NUMBER) AS
    price NUMBER;
    total_amount NUMBER;
  BEGIN
    -- 查询商品价格
    SELECT price INTO price FROM products WHERE product_id = product_id;
    
    -- 计算订单总金额
    total_amount := price * quantity;
    
    -- 更新商品库存数量
    UPDATE products SET quantity = quantity - quantity WHERE product_id = product_id;
    
    -- 插入订单表
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (order_id, customer_id, SYSDATE, total_amount);
    
    -- 插入订单详情表
    INSERT INTO order_details (order_id, product_id, quantity, unit_price)
    VALUES (order_id, product_id, quantity, price);
    
    COMMIT;
  END create_order;
  
  -- 存储过程：取消订单
  PROCEDURE cancel_order(order_id NUMBER) AS
  BEGIN
    -- 删除订单详情表中的记录
    DELETE FROM order_details WHERE order_id = order_id;
    
    -- 删除订单表中的记录
    DELETE FROM orders WHERE order_id = order_id;
    
    COMMIT;
  END cancel_order;
  
  -- 函数：计算订单总金额
  FUNCTION calculate_total_amount(order_id NUMBER) RETURN NUMBER AS
    total_amount NUMBER;
  BEGIN
    -- 查询订单详情表中商品的单价和数量，计算订单总金额
    SELECT SUM(quantity * unit_price) INTO total_amount
    FROM order_details
    WHERE order_id = order_id;
    
    RETURN total_amount;
  END calculate_total_amount;
END sales_pkg;
/
​
CREATE OR REPLACE PACKAGE BODY sales_system_pkg AS
​
    -- 添加新产品
    PROCEDURE add_product(
        p_product_name IN products.product_name%TYPE,
        p_category IN products.category%TYPE,
        p_price IN products.price%TYPE,
        p_stock_quantity IN products.stock_quantity%TYPE,
        p_description IN products.description%TYPE
    ) IS
    BEGIN
        INSERT INTO products (product_id, product_name, category, price, stock_quantity, description)
        VALUES (SEQ_PRODUCT_ID.NEXTVAL, p_product_name, p_category, p_price, p_stock_quantity, p_description);
    END add_product;
​
    -- 添加新客户
    PROCEDURE add_customer(
        p_customer_name IN customers.customer_name%TYPE,
        p_phone IN customers.phone%TYPE,
        p_address IN customers.address%TYPE
    ) IS
    BEGIN
        INSERT INTO customers (customer_id, customer_name, phone, address)
        VALUES (SEQ_CUSTOMER_ID.NEXTVAL, p_customer_name, p_phone, p_address);
    END add_customer;
​
    -- 获取商品总数
    FUNCTION get_total_products RETURN NUMBER IS
        v_total_products NUMBER;
    BEGIN
        SELECT COUNT(*) INTO v_total_products FROM products;
        RETURN v_total_products;
    END get_total_products;
​
    -- 获取客户总数
    FUNCTION get_total_customers RETURN NUMBER IS
        v_total_customers NUMBER;
    BEGIN
        SELECT COUNT(*) INTO v_total_customers FROM customers;
        RETURN v_total_customers;
    END get_total_customers;
​
END sales_system_pkg;